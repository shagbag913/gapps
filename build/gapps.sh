#!/bin/bash
# (c) Joey Rizzoli, 2015
# (c) Paul Keith, 2017
# Released under GPL v2 License

##
# var
#
DATE=$(date -u +%Y%m%d)
TOP=$(cd "$(dirname $0)/../"; pwd -P)
ANDROIDV=9
OUT=$TOP/out
BUILD=$TOP/build
METAINF=$BUILD/meta
UPDATEBINARY=$BUILD/update_binary
PREBUILT=$TOP/arm64/proprietary
ADDOND=$TOP/addond.sh
ZIPNAME=ShagGApps-$ANDROIDV-arm64-$DATE.zip
MD5SUMFILE=$OUT/$ZIPNAME-md5sum.txt
CHANGELOGFILE=$OUT/$ZIPNAME-changelog.txt

##
# functions
#
clean() {
    echo "Cleaning up..."
    rm -r $OUT/arm64
    rm /tmp/$ZIPNAME
    return $?
}

failed() {
    echo "Build failed!"
    exit 1
}

create() {
    echo "Making output directories..."
    [[ -d $OUT ]] || mkdir $OUT
    [[ -d $OUT/arm64 ]] || mkdir -p $OUT/arm64
    [[ -d $OUT/arm64/system ]] || mkdir -p $OUT/arm64/system
    [[ -d $OUT/arm64/system/product/overlay ]] || mkdir -p $OUT/arm64/system/product/overlay
    [[ -d $OUT/arm64/system/addon.d ]] || mkdir -p $OUT/arm64/system/addon.d
    echo "Copying prebuilts..."
    cp -r $PREBUILT/* $OUT/arm64/system
    echo "Copying RRO overlays..."
    for i in $(find overlay/ | grep ShagGAppsOverlay*); do
        cp $i $OUT/arm64/system/product/overlay
    done
    echo "Generating addon.d script..."
    cp -f addond_head $OUT/arm64/system/addon.d
    cp -f addond_mid $OUT/arm64/system/addon.d
    cp -f addond_tail $OUT/arm64/system/addon.d
}

updatebinary() {
    echo "Generating update-binary..."
    {
        echo '#!/sbin/sh'
        echo 'set -x'
        echo 'OUTFD="/proc/self/fd/$2"'
        echo 'ZIP=$3'
        echo ''

        cat $UPDATEBINARY/functions

        echo ''

        # BANNER
        HEIGHT=$(cat $UPDATEBINARY/banner | wc -l)
        for n in $(seq $HEIGHT); do
            echo "ui_print \"$(cat $UPDATEBINARY/banner | sed -n "$n"p)\""
        done

        echo ''

        cat $UPDATEBINARY/main_head
        cat $UPDATEBINARY/gappslist
        cat $UPDATEBINARY/main_tail
    } > $OUT/arm64/META-INF/com/google/android/update-binary
}

zipit() {
    echo "Importing installation scripts..."
    [[ -d $OUT/arm64/META-INF ]] || mkdir $OUT/arm64/META-INF
    echo "Copying Meta..."
    cp -r $METAINF/* $OUT/arm64/META-INF/
    updatebinary
    echo "Creating package..."
    cd $OUT/arm64
    zip -qr /tmp/$ZIPNAME .
    rm -rf $OUT/tmp
    cd $TOP
    if [[ -f /tmp/$ZIPNAME ]];  then
        echo "Signing zip..."
        java -Xmx2048m -jar $TOP/build/sign/signapk.jar -w $TOP/build/sign/testkey.x509.pem $TOP/build/sign/testkey.pk8 /tmp/$ZIPNAME $OUT/$ZIPNAME
    else
        echo "Couldn't find unsigned zip file, aborting"
        return 1
    fi
}

getmd5() {
    if MD5SUM=$(md5sum $OUT/$ZIPNAME | cut -d ' ' -f 1);  then
        echo "md5sum is installed, getting md5..."
        echo -e "$MD5SUM\n" > $MD5SUMFILE
        echo "md5 exported at $MD5SUMFILE"
        return 0
    else
        echo "md5sum is not installed, aborting"
        return 1
    fi
}

createchangelog() {
    if ! LASTBUILDDATE=$(cat $TOP/.last_build 2>/dev/null); then
        echo "$TOP/.last_build not found, not generating changelog."
    elif [[ -z $CHANGELOG ]]; then
        echo "\$CHANGELOG not set, not generating a changelog."
    else
        CHANGELOG="$(git log --pretty='%ad %s %n' --date=short --since="$LASTBUILDDATE")"
        if [[ -n $CHANGELOG ]]; then
            echo "Generating changelog..."
            echo -e "Changes since $LASTBUILDDATE:\n" > $CHANGELOGFILE
            echo -e "$CHANGELOG\n" >> $CHANGELOGFILE
        else
            echo "No changes were found, not generating a changelog."
        fi
    fi
}

##
# main
#
for func in create zipit getmd5 clean createchangelog; do
    if $func; then
        continue
    else
        failed
    fi
done

echo "Build completed!"
echo "---------------------------------------"
echo "md5sum: $MD5SUM"
echo "Package: $OUT/$ZIPNAME"
[[ -n $CHANGELOG ]] && echo "$(date)" > $TOP/.last_build
exit 0
