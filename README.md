# ShagGApps

**ShagGApps for arm64 - Geared towards Pixel devices**

## Goal
For a while I have wanted a GApps package that has zero hassle and is easy to use. MindTheGapps is already like this, however it is very minimum. These GApps aim to be a GApps package that replaces ALL AOSP apps with the apps available in Google's factory images.

This GApps package also sets Pixel Launcher as the default recents implementation, so Pixel Launcher/recents will behave just like the factory image. This is done using RRO overlays in /system/product.

## Configuration

These GApps also support reading a config file in /sdcard/.shaggappsconf. You can set AOSP apps you would like to keep, and GApps you don't want to be installed. [Here is an example config file.](https://del.dog/shaggappsconf.shell). This config keeps BootleggersROM's default launcher, and doesn't install Pixel Launcher.


The config file can also be named: shaggappsconf.txt, .shaggappsconf.txt, or shaggappsconf.


For AOSP_KEEP, valid types are:
LAUNCHER
KEYBOARD
CALENDAR
CAMERA
PHOTOS
CLOCK
EMAIL
CALC
BROWSER
MESSAGE
CONTACTS
DIALER
SEARCH


For GAPPS_EXCLUDE, valid types are:
LAUNCHER
KEYBOARD
CALENDAR
CAMERA
PHOTOS
CLOCK
EMAIL
CALC
BROWSER
MESSAGE
CONTACTS
DIALER
SEARCH


## Contact me

- If you notice an AOSP app (such as a browser, clock, etc) is not getting removed, feel free to email me at sh4gbag913@gmail.com, or contact me on telegram @shagbag913, and I will add support for it to get removed.

## Build

You can compile your GApps package with GNU make

_make clean_
- Remove output directory

_make gapps_arm64_
- compile signed flashable GApps for arm64

## Thanks and credits
 xanaxdroid
- I used his gapps repo as a guide to help out with what overlays are needed

MindTheGapps:
- These GApps are based off of MTG!

NugTowersHQ
- A group of testers that would've made this more difficult if they didn't exist!
